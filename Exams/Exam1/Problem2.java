// This is Exam 1 Problem 2
// Yarelit Mendoza
// Last Modified 9-14-16

import java.util.Scanner;
import java.lang.Math;
import java.text.DecimalFormat;

public class Problem2
{
  public static void main ( String [] args )
  {
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Enter the first of three integer values (a)> ");
    int a = scan.nextInt();
    System.out.println("Enter the scond of three integer values (b)> ");
    int b = scan.nextInt();
    System.out.println("Enter the third of three integer values (c)> ");
    int c = scan.nextInt();
    
    System.out.println("The three integer values you have entered are a = " + a +
                       ", b = " + b + ", and c = " + c + ".");
    
    double x1 = (-b + Math.sqrt(Math.pow(b,2)-(4*a*c)))/(2*a);
    double x2 = (-b - Math.sqrt(Math.pow(b,2)-(4*a*c)))/(2*a);
    
    // Format four decimal places for solution
    DecimalFormat solutionFormat = new DecimalFormat("#0.0000");
    
    
    System.out.println("The first solution x1 is " + solutionFormat.format(x1));
    System.out.println("The second solution x2 is " + solutionFormat.format(x2));
    
    // Find the maximum value
    double max = Math.max(x1, x2);
    double min = Math.min(x1, x2);
    
    // Format six decimal places for max and min
    DecimalFormat maxminFormat = new DecimalFormat("#0.000000");
    System.out.println("The maximum value of the solutions is " + maxminFormat.format(max));
    System.out.println("The minimum value of the solutions is " + maxminFormat.format(min));
    
    
  }
}