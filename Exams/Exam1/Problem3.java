// This is Exam 1 Problem 3
// Yarelit Mendoza
// Last Modified 9-14-16
// Great job!
import java.util.Scanner;
import java.lang.Math;

public class Problem3
{
  public static void main ( String [] args )
  {
    System.out.println("Welcome to the octal converter. We convert three-digit octal numbers to their" +
                       " decimal equivalent. Enter a String containing the three-digit octal number that " +
                       "you want to convert to decimal > ");
    
    // Scanner and System classes to prompt user to enter String containing three octal digits
    Scanner scan = new Scanner(System.in);
    String digit = scan.next();
    
    // Display the user input back to the console
    System.out.println("You have entered the octal number " + digit + "." );
    
    // Use the Integer wrapper class and the Math class to convert this input into an int
    // representing the decimal equivalent
    
    String a = digit.substring(0, 1);
    String b = digit.substring(1, 2);
    String c = digit.substring(2, 3);

    Integer aInt = Integer.parseInt(a);
    Integer bInt = Integer.parseInt(b);
    Integer cInt = Integer.parseInt(c);
    
    int convert = (int)(((aInt * Math.pow(8,2)) + (bInt * Math.pow(8,1)) + (cInt * Math.pow(8,0))));
    
    // Display the int representing the decimal equivalent of the input String representing 
    // an octal number
    // HINT: converting from any base 10 requires linear combinations of coefficents and 
    //       the base raised to incrementing powers starting at 0.
    
    System.out.println("The equivalent of " + digit + " in base 8 is " + convert + " in base 10.");
    
  }
}