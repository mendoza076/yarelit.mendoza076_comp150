// This is Exam 1 Problem 1
// Yarelit Mendoza
// Last Modified 9-14-16

import java.util.Scanner;
import java.lang.Math;

public class Problem1
{
  public static void main ( String [] args )
  {
    // Take one int from user input representing an amount of time in units
    // of millisecond (1 msec - 1/1000 sec ). Use scanner class to read in the input
    Scanner scan = new Scanner(System.in);
    
    System.out.print(" Enter a time in units of milliseconds > " );
    int number = scan.nextInt();
    
    System.out.println("The value you have entered is " + number + " Milliseconds.");
    System.out.println("The value is equivalent to: ");
    
    // Convert this time into the corresponding number of hours, minutes, seconds, and 
    // milliseconds
    
    // msec = 1/1000
 
    
    // 1 millisecond = 1 milliseconds
    // 1000 millisecond = 1 second
    // 60000 milliseconds = 1 minute
    // 3600000 = 1 hour
    
    int hour = number/3600000;
    int hourLeft = number%3600000;
    int minute = hourLeft/60000;
    int minuteLeft = hourLeft%6000;// pretty close.  I think this should be 60000.
    int second = minuteLeft/1000; 
    int secondLeft = minuteLeft%1000;
    int milisecond = secondLeft/1;
    
    System.out.println(hour + " hours + " + minute + " Minutes + " + second + " Seconds + " + milisecond  + " Milliseconds.");
    
  }
}