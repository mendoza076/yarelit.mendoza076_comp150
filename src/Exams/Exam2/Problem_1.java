package Exams.Exam2;

/**
 * Created by yarelit.mendoza076 on 9/27/16.
 */

import java.util.Random;
public class Problem_1
{
    public static void main (String [] args)
    {
        final double THRESHOLD = .0001;
        final double TARGET = 20.0;
        Random random = new Random();

        double end = 20-(.0001); // Should be 20+(1.0/1000) notice that 0.0001 is not 1.0/1000.0
        double start = 20+(.0001); // should be 20-(1.0/1000) you have the sign backwards.

        double randNumber = (end - start + 1) + start; // should be (end-start)*random.nextDouble()+start.

        String message;
        if(Math.abs(TARGET - randNumber) < THRESHOLD) // Good.  Why did you choose this value of for THRESHOLD?
        {
            message = "\nThe number is equal to 20.0";
        }
        else
        {
            message = "\nThe random  number does not equal 20.0. It is off by "; // Include decimal format for Math.abs(TARGET - randNumber)
        }

        System.out.println("The target is " + TARGET + " The random number is " + randNumber + message);


    }
}
