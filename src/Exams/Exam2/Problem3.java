package Exams.Exam2;

/**
 * Created by yarelit.mendoza076 on 9/27/16.
 */

import java.util.Scanner;
public class Problem3
{
    public static void main ( String [] args)
    {
        // Input
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a month of the year (example: September) >");
        String month = scan.next();

        switch(month)
        {
            case "January":
            case "january":
            case "Jan":
            case "jan":
            case "JANUARY":

            case "February":
            case "february":
            case "Feb":
            case "feb":
            case "FEBRUARY":

            case "March":
            case "march":
            case "Mar":
            case "mar":
            case "MARCH":

                System.out.println("The month you entered was " + month + ". This month is in the first quarter.");
                break;
            case "April":
            case "april":
            case "Apr":
            case "apr":
            case "APRIL":

            case "May":
            case "may":
            case "MAY":

            case "June":
            case "june":
            case "Jun":
            case "jun":
            case "JUNE":
                System.out.println("The month you entered was " + month + ". This month is in the second quarter.");
                break;
            case "July":
            case "july":
            case "Jul":
            case "jul":
            case "JULY":

            case "August":
            case "august":
            case "Aug":
            case "aug":
            case "AUGUST":

            case "September":
            case "september":
            case "sep":
            case "Sep":
            case "SEPTEMBER":
                System.out.println("The month you entered was " + month + ". This month is in the third quarter.");
                break;
            case "October":
            case "october":
            case "oct":
            case "Oct":
            case "OCTOBER":

            case "November":
            case "november":
            case "nov":
            case "Nov":
            case "NOVEMBER":

            case "December":
            case "december":
            case "dec":
            case "Dec":
            case "DECEMBER":
                System.out.println("The month you entered was " + month + ". This month is in the fourth quarter.");
                break;
            default:
                System.out.println("Invalid input." + "\n\nThe month you entered was " + month + " This month is not a valid month.");
                break;
        }
    }
}
