package Exams.Exam2;

/**
 * Created by yarelit.mendoza076 on 9/27/16.
 */

import java.util.Scanner;
public class Problem2
{
    public static void main ( String [] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the current car count (should be between 0 and 100)");
        int car = scan.nextInt();
        // if car is invalid then you shouldn't bother to prompt for other inputs.
        System.out.println("Enter the reading from the Entrance sensor >");
        boolean entrance = scan.nextBoolean(); // good.

        System.out.println("Enter the reading from the Exit Sensor >");
        boolean exit = scan.nextBoolean();

        String message = "";
        int parking=0;

        if(car >= 0 && car <= 100)
        {
            car = car;

            if(entrance == true &&  car < 100 && exit == false)
            {
                car = car + 1;
                parking = 100 - car;
                message = "A new car has arrived. \nNow there are " + car + " cars in the lot. " +
                        "\nNow there are " + parking + " spots available.";
            }
            else if (entrance == true && car == 100) // needs additional && !exit 
            {
                message = "A new car has arrived but there are no available spots." +
                        " Please wait. \nNow there are 100 cars in the lot. \nNow there are 0 spots available.";
            }
            else if(car >= 1 && exit == true && entrance == false)
            {
                car = car - 1;
                parking = 100 - car;
                message = "A car has left. \nNow there are " + car + " cars in the lot. " +
                        "\nNow there are " + parking + " spots available.";

            }
            else if(car == 0 && exit == true ) // needs additional && !entrance
            {
                message = "A car gas left but there are no cars in the lot. System error." +
                        "\nNow there are 0 cars in the lot. \nNow there are 100 available spots";
            }
            else if(entrance == true && exit == true)
            {
                car = car;
                parking = car; // There shuld be some message for this case as well.
            }
            System.out.println(message);
        }
        else
        {
            System.out.println("You have entered an invalid current count. " +
                    "The current count must be between zero and the number of spots in the lot");
        }




    }
}
