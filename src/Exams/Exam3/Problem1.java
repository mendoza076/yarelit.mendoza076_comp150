package Exams.Exam3;

/**
 * Created by yarelit.mendoza076 on 10/6/16.
 */

public class Problem1
{
    public static void main ( String [] args )
    {


        int line = 1;
        while (line <= 5) {


            int number = 1;
            while (number <= line)
            {

                System.out.println(number + " "); // This should be System.out.print(number + " ");
                number++;

            }

            line++;

            System.out.println();
        }
    }
}
