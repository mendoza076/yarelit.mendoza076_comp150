package Exams.Exam3;

/**
 * Created by yarelit.mendoza076 on 10/6/16.
 */
import java.util.Scanner;

public class Problem3
{
    public static void main ( String [] args)
    {
        Scanner scan = new Scanner( System.in );
        System.out.println("Enter a word to test for palindrome. > ");
        String word = scan.nextLine();
        // Convert your input to lower case to correctly match Mom as a palindrome.
        String beginning;
        String ending;


        for ( int i = 0; i < word.length(); i++ ) {


            beginning = word.substring(i, word.length() - (word.length() - 1)); // The goal here should be reversing the input String.
            ending = word.substring(word.length() - 1, word.length());

            if (beginning.matches(ending)) { // Move this messaging to outside the for-loop. The conditional should be that the forward input matches the reversed String produced in the for-loop above.
                System.out.println(word + " is a palindrome.");
            } else {
                System.out.println(word + " is not a palindomre.");
            }
        }

        /*int end = 0;
        for( int i = 0; i < word.length(); i++)
        {
            beginning = word.substring(i, word.length()-1);
            ending = word.substring(word.length()-1, word.length());




            System.out.println(beginning);
            System.out.println(ending);

        } */


    }
}
