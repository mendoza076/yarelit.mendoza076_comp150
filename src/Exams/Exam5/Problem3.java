public class Problem3
{
  // single instance variable
  private int [] grades;
  
  // default constructor
  public Problem3()
  {
    grades = new int[0];
  }
  
  // overloaded constructor
  public Problem3( int [] newArray )
  {
    int [] temp = new int [grades.length]; // This should be newArray.length in place of grades.length.
    for( int i = 0; i < grades.length; i++)
    {
      temp[i] = newArray[i];
      grades[i] = temp[i];
    }
  }
  
  // getGrades
  public int [] getGrades()
  {
    int [] temp = new int [grades.length]; // Good.
    for( int i = 0; i < grades.length; i++ )
    {
      temp[i] = grades[i];
    }
    
    return temp;
  }
  
  // setGrades
  public void setGrades(int [] newGrades)
  {
    int [] temp = new int[grades.length]; // Again this should be newGrades.length.
    for( int i = 0; i < grades.length; i++ )
    {
      temp[i] = newGrades[i];
      grades[i] = temp[i];
    }
    
  }
  
  // toString is missing.
  
  public boolean equals(Object o)
  {
    boolean response = false;
    if( !( o instanceof Problem3 ))
    {
      response = false;
    }
    else
    {
      Problem3 obj = (Problem3) o;
      if( grades.length != (obj.grades).length )
      {
        response = false;
      }
      else
      {
        for( int i = 0; i < grades.length; i++)
        {
          if( grades[i] == (obj.grades)[i] )
          {
            response = true;
          }
        }
      }
    }
    
    return response; //edit
  }
  
  public char [] getLetterGrades()
  {
    char grade = ' ';
    char [] temp = new char[grades.length];
    for( int i = 0; i < grades.length; i++ )
    {
      int number = grades[i];
      if( number >= 90 )
      {
        grade = 'A';
      }
      else if( number >= 80 )
      {
        grade = 'B';
      }
      else if( number >= 70 )
      {
        grade = 'C';
      }
      else if ( number >= 60 )
      {
        grade = 'D';
      }
      else
      {
        grade = 'F';
      }
      temp[i] = grade;
    }
    return temp;
  }
  
  public int [] getLetterGradeCount()
  {
    int a = 0;
    int b = 0;
    int c = 0;
    int d = 0;
    int f = 0;
    
    int [] count = new int[grades.length]; // This should be length 5.
    for( int i = 0; i < grades.length; i++ )
    {
      int number = grades[i];
      
      if( number >= 90 )
      {
        a++;
      }
      else if( number >= 80 )
      {
        b++;
      }
      else if( number >= 70 )
      {
        c++;
      }
      else if ( number >= 60 )
      {
        d++;
      }
      else
      {
        f++;
      }
      count[i] = a; // count[0]=a; count[1]=b; count[2]=c; count[3]=d; count[4]=f;
    }
    return count;
  }
  
}