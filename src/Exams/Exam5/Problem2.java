import java.io.IOException;
import java.util.Scanner;
import java.io.File;
import java.lang.Math;

public class Problem2
{
    public static void main ( String [] args) throws IOException
    {
        int number;
        double average = 0;
        int total = 0;
        int count = 0;
        int min = 0;
        String list ="";
        //

        File inputFile = new File("DataLog.txt");
        Scanner file = new Scanner(inputFile);

        while(file.hasNext())
        {
            number = file.nextInt();
            total += number;
            count++; //number of ints
            average = (double)total/count; // This works for computing the average, but the value will only be valid at the end of the file. 
            if( number < average ) // You need to perform this check after average is valid.  To do this you need to sor the contents of the file in an ArrayList and loop back over them to perform this check.
            {
              min = number;
              list+= min + " " ;
            }
            
        }
        
        
        System.out.println("The average of call entries in the file is: " + average +
        "\nThe entries in the file that are less than the average are: " + list );
        
        
        


    }
}
