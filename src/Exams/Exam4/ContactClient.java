package Exams.Exam4;

/**
 * Created by yarelit.mendoza076 on 10/20/16.
 */

import java.util.Scanner;
public class ContactClient
{
    public static void main ( String [] args )
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the name of your contact. > ");
        String name = scan.nextLine();


        System.out.println("Enter the phone number of your contact. Expecting: xxx-xxx-xxxx > ");
        String number = scan.nextLine(); // You should reprompt until the use enters a valid input.

        System.out.println("Enter the email address of your contact. Must contain @ > ");
        String email = scan.nextLine(); // You should reprompt until the use enters a valid input.

        Contact c1  = new Contact(name, number, email);
        System.out.println(c1.toString()); 

        // object 2

        System.out.println("Enter the name of your contact. >");
        String name2 = scan.nextLine();

        System.out.println("Enter the phone number of your contact. Expecting: xxx-xxx-xxxx > ");
        String number2 = scan.nextLine(); // You should reprompt until the use enters a valid input.

        System.out.println("Enter the email address of your contact. Must contain @ > ");
        String email2 = scan.nextLine(); // You should reprompt until the use enters a valid input.

        Contact c2 = new Contact(name2, number2, email2);
        boolean claim = c1.equals(c2); // Good.
        if( claim = false ) // You have used the = operator (assigment). You don't need an operator for a boolean.  It should just be if(claim) { }
        {
            System.out.println("The two contacts are not equal");

        }
        else
        {
            System.out.println("The two contacts are equal.");
        }
    }
}
