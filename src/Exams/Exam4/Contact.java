package Exams.Exam4;

import java.rmi.activation.ActivationGroup_Stub;

/**
 * Created by yarelit.mendoza076 on 10/20/16.
 */
public class Contact
{
    private String name;
    private String phoneNumber;
    private String emailAddress;

    public Contact()
    {
        name = "unknown";
        phoneNumber = "unknown";
        emailAddress = "unknown";
    }

    public Contact(String newName,
                   String newPhoneNumber,
                   String newEmailAddress)
    {
        setName(newName);
        setPhoneNumber(newPhoneNumber);
        setEmailAddress(newEmailAddress);
    }

    public String getName()
    {
        String temp = name;
        return temp;
    }

    public String getPhoneNumber()
    {
        String temp = phoneNumber;
        return temp;
    }

    public String getEmailAddress()
    {
        String temp = emailAddress;
        return temp;
    }

    public void setName( String newName )
    {
        name = newName;
    }

    public void setPhoneNumber( String newPhoneNumber )
    {
        String numberFormat = "[0-9]{3}-[0-9]{3}-[0-9]{4}";
        String temp = newPhoneNumber;

        if (!(newPhoneNumber.matches(numberFormat)))
        {
            phoneNumber = "incorrect format";
        }
        else
        {
            phoneNumber = temp;
        }
    }

    public void setEmailAddress( String newEmailAddress )
    {
        String emailFormat = "[a-zA-Z]*[.]*[a-zA-Z]*[@][a-zA-Z]*[.][a-zA-Z]*";
        String temp = newEmailAddress;

        if (newEmailAddress.matches(emailFormat))
        {
            emailAddress = temp;
        }
        else
        {
            emailAddress = "incorrect format";
        }
    }

    public String toString()
    {
        return "The contact you have entered is: " + "\nname: " + name + "\nphone: " + phoneNumber
                + "\nemail: " + emailAddress;
    }

    public boolean equals( Object o )
    {
        if ( ! (o instanceof Contact) )
        {
            return false;
        }
        else
        {
            Contact objContact = ( Contact ) o;
            return ( name.matches(objContact.name) // For Objects you should use the .equals method to compare them.
                    && phoneNumber.matches(objContact.phoneNumber)
                    && emailAddress.matches(objContact.emailAddress));
        }

    }
}
