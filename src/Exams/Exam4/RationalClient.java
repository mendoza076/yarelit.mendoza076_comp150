package Exams.Exam4;

/**
 * Created by yarelit.mendoza076 on 10/20/16.
 */
import java.util.Scanner;
public class RationalClient
{
    public static void main (String [] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.println(("Enter your desired operation: >"));
        String operation = scan.nextLine();

        System.out.println("Enter your first rational number: (required format a/b >");
        String rationalOne = scan.nextLine();
        int a = Integer.parseInt(rationalOne.substring(0,1));
        int b = Integer.parseInt(rationalOne.substring(2,3));

        Rational r1 = new Rational(a,b);

        System.out.println("Enter your second rational number (required format: a/b >");
        String rationalTwo = scan.nextLine();
        int a2 = Integer.parseInt(rationalTwo.substring(0,1));
        int b2 = Integer.parseInt(rationalTwo.substring(2,3));

        Rational r2 = new Rational(a2,b2);

        System.out.println(r1.addition(r2)); // Your client should perform the operation that was provided by the user in line 13.

    }
}
