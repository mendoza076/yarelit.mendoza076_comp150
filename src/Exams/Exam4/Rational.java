package Exams.Exam4;

/**
 * Created by yarelit.mendoza076 on 10/20/16.
 */

import java.lang.Math;
public class Rational
{
    private int numerator;
    private int denominator;

    public Rational()
    {
        numerator = 0;
        denominator = 0; // denominator should never be allowed to be 0.
    }

    public Rational(int newNumerator,
                    int newDenominator)
    {
        setNumerator(newNumerator);
        setDenominator(newDenominator);
    }

    public int getNumerator()
    {
        int temp = numerator;
        return temp;
    }

    public int getDenominator()
    {
        int temp = denominator;
        return temp;

    }

    public void setNumerator(int newNumerator)
    {
        int temp = newNumerator;
        numerator = temp;
    }

    public void setDenominator(int newDenominator)
    {
        int temp = newDenominator;
        if (newDenominator == 0) // Good.
        {
            denominator = 1;
        }
        else
        {
            denominator = temp;
        }
    }

    public String toString()
    {
        return ""; // This could have been as simple as: return numerator + "/" + denominator;
    }

    public boolean equals(Object o)
    {
        if( !(o instanceof Rational))
        {
            return false;
        }
        else
        {
            Rational objRational = (Rational) o;
            return (numerator == objRational.numerator
                    && denominator == objRational.denominator);
        }
    }




    public boolean isInteger()
    {

        return false; // edit // Check to see if denominator == 1.
    }



    public Rational addition ( Rational r )
    {
        int ad = numerator * r.denominator;
        int bc = denominator * r.numerator;
        int bd = denominator * r.denominator;

        Rational add = new Rational(ad + bc, bd); // Good.


        return add;
    }

    public Rational subtraction ( Rational r )
    {
        int ad = numerator * r.denominator;
        int bc = denominator * r.numerator;
        int bd = denominator * r.denominator;

        Rational subtract = new Rational(ad - bc, bd); // Good.
        return subtract;
    }

    public Rational multiplication ( Rational r )
    {
        int ac = numerator * r.numerator;
        int bd = denominator * r.denominator;

        Rational multiply = new Rational(ac, bd); // Good.
        return multiply;
    }

    public Rational division ( Rational r)
    {
        int ad = numerator * r.denominator;
        int bc = denominator * r.numerator;

        Rational divide = new Rational(ad,bc); // Good.

        return divide;
    }

    public Rational inverse () // This method should be void.
    {
        Rational inverse = new Rational (denominator,numerator);
        return inverse;
    }

    public Rational negation () // This method should be void.
    {
        Rational negate = new Rational (numerator,-denominator);
        return negate;
    }

    public Rational pow (int p) // This method should be void.
    {
        if ( p > 0)
        {
            int a = (int) Math.pow(numerator, p);
            int b = (int) Math.pow(denominator, p);

            Rational pow = new Rational(a,b);
            return pow;

        }
        else
        {
            int a = (int) Math.pow(numerator, p);
            int b = (int) Math.pow(denominator, p);

            Rational pow = new Rational (b, a);

            return pow;
        }

    }
}
