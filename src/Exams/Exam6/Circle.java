//Yarelit M.
// problem 2
//

public class Circle extends Shape
{
  private double radius;
  
  public Circle()
  {
    radius = 0.0;
  }
  
  public Circle( double startRadius)
  {
    radius = startRadius;
  }
  
  public String toString()
  {
    return super.toString() + "Radius: " + radius;
  }

  // area pi*radius^2
  // 2*pi*r

  public double area()
  {
    return super.area() + Math.PI * Math.pow(radius, 2); //you don't need the super.area() part.
  }

  public double perimeter()
  {
    return super.perimeter() + 2*Math.PI*radius; //you don't need the super.perimeter() part.
  }

}
  