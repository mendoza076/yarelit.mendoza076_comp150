// Yarelit M
// Problem 1

import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;

public class Problem1
{
  public static void main (String [] args)
  {
    String first = "";
    String name = "";
    String last = "";
    String lastName = "";
    String id = "";
    String idString = "";
    String password = "";
    String passwordString = "";
    
    try
    {
      Scanner scan = new Scanner(new File ("inputTextFile.txt"));
      while( scan.hasNext())
      {
        String line = scan.nextLine();
        Scanner parse = new Scanner(line);
        parse.useDelimiter("&");
        
        String a = parse.next();
        String b = parse.next();
        String c = parse.next();
        String d = parse.next();
        
        Scanner parseA = new Scanner(a);
        parseA.useDelimiter("=");
        first = parseA.next();
        name = parseA.next();
        
        System.out.println(first + ": " + name);
        
        Scanner parseB = new Scanner(b);
        parseB.useDelimiter("=");
        last = parseB.next();
        lastName = parseB.next();
        
        System.out.println(last + ": " + lastName); // Great job on this problem!
        
        Scanner parseC = new Scanner(c);
        parseC.useDelimiter("=");
        id = parseC.next();
        idString = parseC.next();
        
        System.out.println(id + ": " + idString);
        
        Scanner parseD = new Scanner(d);
        parseD.useDelimiter("=");
        password = parseD.next();
        passwordString = parseD.next();
        
        System.out.println(password + ": " + passwordString);
        
        
        
      }
      
      scan.close();
    }
    
    catch( FileNotFoundException fnfe )
    {
      System.out.println("Unable to find transaction.txt");
    }
    catch( Exception e )
    {
      e.printStackTrace(); // prints all the errors found
    }
    
    /* System.out.println(first + ": " + name + "\n" + last + ": " + lastName + "\n" + id + ": " + idString 
     + "\n" + password + ": " + passwordString); */
    
  }
  
}