// Yarelit Mendoza
// Problem 3

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileNotFoundException;
import java.io.EOFException;
import java.io.IOException;

public class Problem3
{
  public static void main(String[] args) // This class should not contain a main method.  It should only contain two static methods.  I have provided the client class with the main method.
  {
    try{
    FileInputStream fis = new FileInputStream("inputObjectFileOfAutos");
    ObjectInputStream ois = new ObjectInputStream(fis);

      try {
        while (true) {
          Auto temp = (Auto) ois.readObject();
          System.out.println(temp); // Now that you read the Auto object out of the file you can use the get methods to create a comma delimited string containing the data from that object.  Use print writer to write the human readable text file.
        }
      } catch (EOFException eofe) {
        System.out.println("End of the file reached");
      } catch (ClassNotFoundException cnfe) {
        System.out.println(cnfe.getMessage());
      } finally {
        System.out.println("Closing file");
        ois.close();
      }
    } catch (FileNotFoundException fnfe) {
      System.out.println("Unable to find objects");
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }
}