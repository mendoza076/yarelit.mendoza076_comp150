// Yarelit M
// Problem 2
//

import java.lang.Math;

public abstract class Shape
{
  final private double pi = Math.PI;
  private double perimeter;
  private double area;
  
  
  // methods
  public double perimeter() // This method should abstract.  No body.
  {
    return perimeter;
  }
  
  public double area() // This method should abstract.  No body.
  {
    return area;
  }
  

  public String toString()
  {
    return "";
  }
}