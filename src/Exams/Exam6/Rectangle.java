// Yarelit Mendoza
// Problem 2
//

public class Rectangle extends Shape
{
  private double width;
  private double length;
  
  public Rectangle()
  {
    width = 0;
    length = 0;
  }
  
  public Rectangle( double startWidth, double startLength )
  {
    width = startWidth;
    length = startLength;
  }
  
  public double perimeter()
  {
    double solution = 2*(width*length);
    return super.perimeter() + solution; //you don't need the super.perimeter() part.
  }
  
  public double area()
  {
    double solution = width*length;
    return super.area() + solution; //you don't need the super.area() part.
  }

  public String toString()
  {
    return super.toString() + "Width: " + width + "\nLength: " + length;
  }
}