// Modified by Jason Isaacs 05/09/2016
// Test client for Shape Abstract Superclass and and Circle and Rectangle Subclasses
//

public class ShapeClient
{
    public static void main (String[] args)
    {

        Shape shape1 = new Circle(10.0);
        Shape shape2 = new Rectangle(2.0, 3.0);
        
        System.out.println(shape1.toString());
        System.out.println();
        System.out.println(shape2.toString());
        System.out.println();
        System.out.println("Circle: Area: " + shape1.area());
        System.out.println("Circle: Perimeter: " + shape1.perimeter());
        System.out.println();
        System.out.println("Rectangle: Area: " + shape2.area());
        System.out.println("Rectangle: Perimeter: " + shape2.perimeter());
    }
}
