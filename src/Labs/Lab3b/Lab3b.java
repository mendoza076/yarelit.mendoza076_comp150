// Lab 3b
// Yarelit Mendoza
// Last modified 09-13-16

// import stuff
import java.util.Scanner;
import java.text.NumberFormat;
import java.util.Random;

public class Lab3b
{
  public static void main ( String [] args )
  {
    // A. Prompt the user to enter a dollar amount in the format $xxxx.xx
    System.out.println("Please enter a dollar amount in the format: $xxxx.xx >");
    
    // B. Use tools from the scanner class to read this input String
    Scanner scan = new Scanner(System.in);
    String dollar = scan.next();
    
    // C. Display the input String to console
    System.out.println("You entered: " + dollar);
    
    // D. Extract two strings. first containing the number of dollars and second contaning number of
    //   cents. Note: The dollar amount can be any number of dollars, but the number of cents will always be 
    //   between 00 and 99. The dollar amount will come between the �$� and the �.� and the number cents will 
    //   always come after
    
    String dosh = dollar.substring(1, dollar.length()-3);
    int dot = dollar.indexOf('.');
    String cents = dollar.substring(dot+1, dollar.length());
    
    // E. Display the number of dollars and cents to the console
    System.out.println("This is equivalent to " + dosh + " dollars and " + cents + " cents.");
    
    // F. Prompt the user to enter an integer number of additional dollar
    System.out.println("Enter an integer number of additional dollars >");
    
    // G. Use tool from the scanner class the read this input as an int
    int additional = scan.nextInt();
    
    // H. Display the number of additional dollar to console
    System.out.println("You entered " + additional + " additional dollars.");
    
    // I. Use too from random class to generate a random int number of additional cents
    Random random = new Random();
    int start = 0;
    int end = 10;
    int newCents = random.nextInt( end - start + 1) + start;
    
    // J. Display number of additional cents to console
    System.out.println("An additional " + newCents + " cents were generated randomly.");
    
    // K. Sum up the dollars from steps e and h. HInt: You will need to use the integer wrapper class
    Integer iDollar = Integer.parseInt(dosh);
    int dollarSum = iDollar + additional;
    
    // L. Sum up the cents from steps e and j. Hint: You will need to use the Integer wrapper class
    Integer iCents = Integer.parseInt(cents);
    int centsSum = iCents + newCents;
    
    // M. Display these int values from steps k and I to console
    System.out.println("After summing the dollars and cents we have " + dollarSum + " dollars and " 
                         + centsSum + " cents.");
                         
    // N. Convert new dollars and cents int values to single double variable. Hint: You will need to use
    //    explicit type casting
   double newMoney = ((double)dollarSum) + ((centsSum*.01));
    
    // O. Display this new double from step m as currency methods from the NumberFormat class
   NumberFormat priceFormat = NumberFormat.getCurrencyInstance();
   
   System.out.println("The value written in Currency Number Format is: " + priceFormat.format(newMoney));
  }
}