package Labs.Project1;

/**
 * Created by yarelit.mendoza076 on 9/20/16.
 */

import java.util.Scanner;
public class Project1
{
    public static void main ( String [] args)
    {

        // Read from keyboard

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a link >");

        String input = scan.next();
        String address = input.substring(input.length()-3, input.length());

        String message = "";
        if(address.equals("gov"))
        {
            message = "It is a government web address";
        }
        else if(address.equals("edu"))
        {
            message = "It is a university web address";
        }
        else if(address.equals("com"))
        {
            message = "It is a business web address";
        }
        else if(address.equals("org"))
        {
            message = "It is an organization web address";
        }
        else
        {
            message = "It is a web address for another entity";
        }


        System.out.println("You link you have entered is: " + input + "\nThe web address is: " + address + "\n" + message);

    }

}
