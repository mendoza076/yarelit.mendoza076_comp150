public class Player
{
  // instance variables
  private String name;
  private String position;
  private int shotsTaken;
  private int shotsMade;
  
  public Player()
  {
    name = "";
    position = "";
  }
  
  public Player(String newName,
                int newShotsTaken,
                int newShotsMade)
  {
    name = newName;
    shotsTaken = newShotsTaken;
    shotsMade = newShotsMade;
  }
  
  public String getName()
  {
    return name;
  }
  
  public String getPosition()
  {
    return position;
  }
  
  public int getShotsTaken()
  {
    return shotsTaken;
  }
  
  public int getShotsMade()
  {
    return shotsMade;
  }
  
  public void setName(String newName)
  {
    name = newName;
  }
  
  public void setPosition(String newPosition)
  {
    position = newPosition;
  }
  
  public void setShotsTaken(int newShotsTaken)
  {
    shotsTaken = newShotsTaken;
  }
  
  public void setShotsMade(int newShotsMade)
  {
    shotsMade = newShotsMade;
  }
  
  public String toString()
  {
    return "";
  }
  
  public boolean equals( Object o)
  {
    if(!(o instanceof Player))
    {
      return false;
    }
    else
    {
      Player objPlayer = (Player) o;
      return(name.matches(objPlayer.name)
              && position.matches(objPlayer.position)
              && shotsTaken == objPlayer.shotsTaken
              && shotsMade == objPlayer.shotsMade);
    }
  }
  
  public double percentage()
  {
    return 0.0; //adjust
  }
  
}