public class BasketballTeam
{
  private Player[] team;

  public BasketballTeam()
  {
    team = new Player[5];
    for(int i = 0; i < team.length; i++)
    {
      team[i] = new Player();
    }
  }

  /* public BasketballTeam(Player[] newTeam )
  {
    team = new Player[newTeam.length];
    for( int i = 0; i < team.length; i++)
    {
      team[i] = new Player(newTeam.getName,
                           newTeam.getPosition,
                           newTeam.getShotsTaken,
                           newTeam.getShotsMade);
    }
  } */


  public Player[] getTeam()
  {
    Player[] temp = new Player[team.length];
    temp = team; // You still need to copy the data over element by element in a for-loop.
    return temp;
  }

  public void setTeam( Player[] newTeam)
  {
    Player[] temp = new Player[team.length];
    for( int i = 0; i < team.length; i++ )
    {
      temp[i] = newTeam[i];
    }
  }

  public String toString()
  {
    Player[] temp = new Player[team.length];
    int i;
    for( i = 0; i < team.length; i++)
    {
      temp[i] = team[i];
    }

    return "Given team is: " + temp[i]; // You should be concatenating the return STring inside the for-loop.
  }

 /* public boolean equals(BasketballTeam o)
  {
    if(!(o instanceof BasketballTeam))
    {
      return false;
    }
    else
    {
      BasketballTeam  objBasket = (BasketballTeam) o;
      if(!( team.length == o.length))
      {
        return false;
      }
      else
      {
        for(int i = 0; i < team.length; i++)
        {
          if((team[i]).matches(o[i]))
          {
            return true;
          }
        }
      }
    }
  }

  */


  public boolean checkPositions()
  {
    Player[] team = getTeam();
    String tempPosition = team[0].getPosition();
    boolean temp = true;
    for (int i = 0; i < team.length-1; i++)
    {
      if (! ( tempPosition.equals(team[i + 1].getPosition())))
        return false;
    }
    return true;
  }

  public double percentage()
  {
    double shotsMade = 0.0, shotsTaken = 0.0;
    for (int i = 0; i < team.length-1; i++)
    {
      shotsMade += team[i].getShotsMade();
      shotsTaken += team[i].getShotsTaken();
    }
    return ((shotsMade/shotsTaken)*100);

  }

  public boolean checkForCenter()
  {
    for (int i = 0; i < team.length-1; i++)
    {
      if (team[i].getPosition().equals("Center"))
        return true;
    }
    return false;
  }
/*
  public void sortOnShots()
  {
    int temp;
    for( int i = 0; i < team.length; i++)
    {
       for( int j = 0; j < team.length - i; j++)
           {
               if( team[j] > team[j+1])
               {
                   temp = team[j];
                   team[j] = team[j+1];
                   team[j+1] = temp;
               }
           }
    }

  }
  */

  public Player[] sortOnName()
  {
    return team; // change
  }

  public String nameOfMostShots()
  {
    return ""; // change
  }

  public String nameOfHighestShootingAvg()
  {
    return ""; // change
  }

}