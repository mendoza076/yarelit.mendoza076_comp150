package Labs.Project2;

/**
 * Created by yarelit.mendoza076 on 10/18/16.
 */
public class Complex
{
    private double real;
    private double imaginary;

    public Complex()
    {

    }
    //overloaded constructor
    public Complex(double startReal, double startImaginary)
    {
        real = startReal;
        imaginary = startImaginary;
    }

    //accessor method
    //returns current value
    public double getReal()
    {
        return real;
    }


    public double getImaginary()
    {
        return imaginary;
    }
    //mutator method

    public void setReal(double newReal)
    {
        real = newReal;
    }

    public void setImaginary(double newImaginary)
    {
        imaginary = newImaginary;

    }

    public String toString(String comment) // This should take no arguments.
    {
        return "comment"; // This should return a String containing the data of the class. 

    }

    public boolean equals( Object o)
    {
        if (! (o instanceof Complex) )
            return false;
        else
        {
            Complex objComplex = (Complex) o;
            return ((Math.abs(real-objComplex.real) < 0.0001)
                    && (Math.abs(imaginary-objComplex.imaginary) < 0.0001));


        }
    }
 /*
  public boolean isReal()
  {


 }
  */
}