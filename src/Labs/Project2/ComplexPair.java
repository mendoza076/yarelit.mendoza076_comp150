package Labs.Project2;

/**
 * Created by yarelit.mendoza076 on 10/18/16.
 */
public class ComplexPair
{
    private Complex first;
    private Complex second;

    public ComplexPair()
    {
     // Here you should use the default constructor from the Complex class.
    }

    public ComplexPair(Complex newFirst,
                       Complex newSecond)
    {
        setFirst(newFirst);
        setSecond(newSecond);
    }

    public Complex getFirst()
    {
        return first; // This violates encapsulation.
    }

    public Complex getSecond()
    {
        return second;// This violates encapsulation.
    }

    public void setFirst( Complex newFirst )
    {
        first = newFirst;// This violates encapsulation.
    }

    public void setSecond ( Complex newSecond )
    {
        second = newSecond;// This violates encapsulation.
    }

    public String toString()
    {
     // Incomplete
    }

    public boolean equals( Object o )
    {
        ComplexPair objComp = (ComplexPair) o;
        return ( a ==  objComp.first
                && b == objComp.second); // Here you are comparing the object references and not the data.
    }

    public boolean bothIdentical()
    {
 // Incomplete
    }

}
