package Labs.Project2;

/**
 * Created by yarelit.mendoza076 on 10/18/16.
 */

import java.lang.Math;
public class Quadratic
{
    private int a;
    private int b;
    private int c;
    private String comment;

    /**
     * Default constructor:<BR>
     * initialized comment to an empty string <BR>
     * integers a, b, and c are autoinitialized to 0.
     */

    public Quadratic()
    {
        comment = "";
    }

    /**
     * Overloaded constructor:<BR>
     * allows client to set beginning values for a, b, and c <BR>
     * This constructor takes three parameters <BR>
     *
     *@param startA the number of a
     *@param startB the number of b
     *@param startC the number of c
     *
     */

    public Quadratic(int startA, int startB, int startC)
    {
        a = startA;
        b = startB;
        c = startC;
    }

    /**
     * accessor method: <BR>
     * @return the value of a
     */

    public int getA()
    {
        return a;
    }

    /**
     * Mutator method: <BR>
     * allows client to set value of a <BR>
     * @param newA the new number of a
     */

    public void setA(int newA)
    {
        a = newA;
    }

    /**
     * accessor method: <BR>
     * @return value of b
     */

    public int getB()
    {
        return b;
    }

    /**
     * Mutator method: <BR>
     * allows client to set value of b <BR>
     *@param newB the new number of b
     */

    public void setB(int newB)
    {
        b = newB;
    }

    /**
     * accessor method: <BR>
     * @return value of c
     */

    public int getC()
    {
        return c;
    }

    /**
     * Mutator method: <BR>
     * allows client to set value of c <BR>
     * @param newC the new number of c
     */

    public void setC(int newC)
    {
        c = newC;
    }

    /**
     * Accessor method: <BR>
     * @return the string comment
     */

    public String getComment()
    {
        return comment;
    }

    /**
     * toString method: <BR>
     * @return string
     */

    public String toString()
    {
        return "Quadratic equation: ";
    }

    /**
     * Equals method: <BR>
     * Compares two Quadratic objects for the same field values
     * @param o another object
     * @return a boolean, true if o is a Quadratic object
     * and has the same field as this object
     */

    public boolean equals (Object o)
    {
        if( !( o instanceof Quadratic ))
        {
            return false;
        }
        else
        {
            Quadratic objQuad = (Quadratic) o;
            return ( a ==  objQuad.a
                     && b == objQuad.b
                     && c == objQuad.c);
        }
    }

    /**
     * Discriminant method: <BR>
     * @return
     */

    private int discriminant()
    {
        int disc = (int)Math.pow(b,2) - 4*a*c;
        return disc;
    }

    /**
     *
     */

    // code goes here

    /**
     * solveQuadratic method: <BR>
     */

    // public ComplexPair solveQuadratic()
    //{

    //}

}
