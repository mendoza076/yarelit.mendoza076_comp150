package Labs.Lab6b;

/**
 * Created by yarelit.mendoza076 on 10/4/16.
 */

import java.util.Scanner;

public class Lab6b
{
    public static void main ( String [] args )
    {
        int score = Integer.MAX_VALUE;
        int total =0;
        double average =0;
        int count;

        // Read 5 int exam scores from console
        // scores should be 0 and 100 inclusive
        // use nested for-loop and do-while loop combination to read in and validate these scores
        //
        // and compute the class exam average.

        Scanner scan = new Scanner(System.in);
        for (int i = 1; i <= 5; i++)
        {
            count = i;
            do
            {
                System.out.println("Enter test score number " + i + ". >");
                if(scan.hasNextInt())
                {
                    score = scan.nextInt();
                    if(score < 0 || score > 100)
                    {
                        System.out.println("You have entered the integer: " + score);
                    }
                    else
                    {
                        System.out.println("You have entered the integer: " + score +
                                "\nThis is a valid integer between 0 and 100");
                    }
                }
                String junkString = scan.nextLine();

         /* System.out.println("You have entered the String: " + junkString +
                             "\nThis input is not a valid integer."); */

            }while(!(score >= 0 && score <= 100));

            total += score;
            average = (double) total / i;

        }
        System.out.println("\nThe class average for this exam was: " + average);
    }
}
