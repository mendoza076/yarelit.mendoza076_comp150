package Labs.Lab6a;

import java.io.IOException;
import java.io.File;
import java.util.Scanner;

/**
 * Created by yarelit.mendoza076 on 9/29/16.
 */
public class Lab6a
{
    public static void main ( String [] args) throws IOException
    {
        //
        int number;

        File inputFile = new File("src/Labs/Lab6a/inputs.txt");
        Scanner file = new Scanner(inputFile);

        int sum = 0;
        int count = 0;
        double average = 0;
        int max = 0;
        int min = 0;

        while(file.hasNext())
        {
            number = file.nextInt();

            count++;
            sum += number;
            average = (double)sum/count;
            max = Math.max(number, max);
            min = Math.min(number,min);
        }
        System.out.println("The sum is: " + sum + "\nThe total count is: " + count + "\nThe average " +
                "is: " + average + "\nThe max number is: " + max + "\nThe minimum number is: " + min);
    }
}
