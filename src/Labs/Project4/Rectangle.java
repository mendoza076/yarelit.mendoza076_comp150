package Labs.Project4;

/**
 * Created by yarelit.mendoza076 on 11/22/16.
 */
import java.awt.Graphics;
public class Rectangle extends Shape
{
    //
    // instance variables
    int width;
    int height;

    // overloaded constructor
    public Rectangle( Coordinate coordinate,
                      int startWidth,
                      int startHeight)
    {
        super(coordinate);
        width = startWidth;
        height = startHeight;
    }

    // get methods
    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    // set methods
    public void setWidth(int newWidth)
    {
        width = newWidth;
    }

    public void setHeight(int newHeight)
    {
        height = newHeight;
    }

    // toString method
    public String toString()
    {
        return "The width of the rectangle is: " + width + "\nThe height of the rectangle is: " + height;
    }

    public boolean equals(Object o)
    {
        boolean result = false;
        if(!(o instanceof Rectangle))
        {
            result = false;
        }
        else
        {
            Rectangle objRect = (Rectangle) o;
            if(width == objRect.width &&
                    height == objRect.height)
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        return result;
    }

    public void draw(Graphics g)
    {
        g.fillRect(super.getStart().getX(), super.getStart().getY(), width, height);
    }
}
