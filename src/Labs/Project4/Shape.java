package Labs.Project4;

/**
 * Created by yarelit.mendoza076 on 11/22/16.
 */
import java.awt.Graphics;
public abstract class Shape
{
    //
    // instance variable
    Coordinate start;

    // overloaded constructor
    public Shape(Coordinate startCoordinate)
    {
        start = startCoordinate;
    }

    // get method
    public Coordinate getStart()
    {
        Coordinate temp = start;
        return temp;
    }

    // set method
    public Coordinate setStart(Coordinate newStart)
    {
        Coordinate temp = start;
        temp = newStart;
        return temp;
    }

    // toString method
    public String toString()
    {
        return "The start coordinates are: " + start;
    }

    // equals method
    public boolean equals(Object o)
    {
        boolean result = false;
        if(!(o instanceof Shape))
        {
            result = false;
        }
        else
        {
            Shape objShape = (Shape) o;
            if(start == objShape.start)
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        return result;
    }

    // draw method
    public abstract void draw(Graphics g);
}
