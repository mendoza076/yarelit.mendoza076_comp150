package Labs.Project4;

/* An application allowing the user to draw shapes
   This application does not take into account the insets of the window
   Anderson, Franceschi
*/

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.Graphics;

public class DrawShapes extends JFrame
{
  //
  public static final String legalLetters = "LlRrOo";
  private char choice;
  private Shape s;

  public DrawShapes( )
  {
    super( );
    // get shape from user
    String choiceS;
    do
    {
      choiceS = JOptionPane.showInputDialog( null, "Enter L for Line, R for Rectangle, or O for Oval" );
    } while ( legalLetters.indexOf( choiceS.charAt( 0 ) ) == -1 );
    choice = choiceS.charAt( 0 );

    // get data for shape chosen by user
    if ( choice == 'L' || choice == 'l' )
    {
      // get data for line
      // get x value of first coordinate
      int x1 = UserDialogInput.readInteger( "Enter the x coordinate of the first point" );
      // get y value of first coordinate
      int y1 = UserDialogInput.readInteger( "Enter the y coordinate of the first point" );
      // get x value of second coordinate
      int x2 = UserDialogInput.readInteger( "Enter the x coordinate of the second point" );
      // get y value of second coordinate
      int y2 = UserDialogInput.readInteger( "Enter the y coordinate of the second point" );
      // instantiate a line and assign it to s
      s = new Line( new Coordinate( x1, y1 ), new Coordinate( x2, y2 ) );
    }
    else // rectangle or oval
    {
      // get data for rectangle or oval
      // get x value of top-left coordinate
      int xTL = UserDialogInput.readInteger( "Enter the x coordinate of the top left corner" );
      // get y value of top-left coordinate
      int yTL = UserDialogInput.readInteger( "Enter the y coordinate of the top left corner" );
      // get value for the width
      int w = UserDialogInput.readInteger( "Enter the width" );
      // get value for the height
      int h = UserDialogInput.readInteger( "Enter the height" );
      // instantiate a rectangle or an oval and assign it to s
      if ( choice == 'R' || choice == 'r' )
        s = new Rectangle( new Coordinate( xTL, yTL ), w, h );
      else
        s = new Oval( new Coordinate( xTL, yTL ), w, h );
    }
  }

  public void paint( Graphics g )
  {
    super.paint( g );
    // draw the shape chosen by the user
    s.draw( g );
  }
  
  public static void main( String [] args )
  {
    DrawShapes app = new DrawShapes( );
    app.setSize( 400, 300 );
    app.setVisible( true );
  }
}