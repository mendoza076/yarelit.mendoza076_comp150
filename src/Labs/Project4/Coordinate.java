package Labs.Project4;

/**
 * Created by yarelit.mendoza076 on 11/22/16.
 */
public class Coordinate
{
    //
    // instance variables
    private int x;
    private int y;

    // constructor
    public Coordinate()
    {

    }

    // overloaded constructor
    public Coordinate(int startX,
                      int startY)
    {
        x = startX;
        y = startY;
    }

    // get X method
    public int getX()
    {
        return x;
    }

    // get Y method
    public int getY()
    {
        return y;
    }

    // set method (x)
    public void setX(int newX)
    {
        x = newX;
    }

    // set method (y)
    public void setY(int newY)
    {
        y = newY;
    }

    // toString method
    public String toString()
    {
        return "The x coordinate is: " + x + "\nThe y coordinate is: " + y;
    }

    // equals method
    public boolean equals(Object o)
    {
        boolean solution = false;
        if (!(o instanceof Coordinate))
        {
            solution = false;
        }
        else
        {
            Coordinate objCoo = (Coordinate) o;
            if(x == objCoo.x && y == objCoo.y)
            {
                solution = true;
            }
            else
            {
                solution = false;
            }
        }
        return solution;

    }
}
