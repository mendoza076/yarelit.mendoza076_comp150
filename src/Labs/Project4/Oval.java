package Labs.Project4;

/**
 * Created by yarelit.mendoza076 on 11/22/16.
 */
import java.awt.Graphics;
public class Oval extends Shape
{
    //
    // instance variables
    int width;
    int height;

    // overloaded constructor
    public Oval ( Coordinate one, int startWidth, int startHeight)
    {
        super(one);
        width = startWidth;
        height = startHeight;
    }

    // get method
    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    // set methods
    public void setWidth(int newWidth)
    {
        width = newWidth;
    }

    public void setHeight(int newHeight)
    {
        height = newHeight;
    }

    // toString method
    public String toString()
    {
        return "The width of the oval is: " + width + "\nThe height of the oval is: " + height;
    }

    // equals method
    public boolean equals(Object o)
    {
        boolean result;
        if(!(o instanceof Oval))
        {
            result = false;
        }
        else
        {
            Oval objOval = (Oval) o;
            if(width == objOval.width
                    && height == objOval.height)
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        return result;
    }

    public void draw(Graphics g)
    {
        g.fillOval(super.getStart().getX(), super.getStart().getY(), width, height);
    }
}
