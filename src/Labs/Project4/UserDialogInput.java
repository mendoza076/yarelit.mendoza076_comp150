package Labs.Project4;
/** UserDialogInput class
*   Anderson, Franceschi
*/

import javax.swing.JOptionPane;

public class UserDialogInput
{
  //
  /** readInteger method
  *   @param    prompt  message for user
  *   @return   the value read
  */
  public static int readInteger( String prompt )
  {
    int result = 0;
    String message = "";

    String str = "";
    boolean validInt = false;

    do
    {
      // prompt user and read value
      str = JOptionPane.showInputDialog( null, message + prompt );

      try
      {
        // attempt to convert to an integer
        result = Integer.parseInt( str );
        validInt = true;
      }

      catch( NumberFormatException nfe )
      {
        message = "Invalid integer:  ";
      }
    } while ( !validInt );

    return result;
  }

 // other methods of the UserDialogInput class
 // ...
}
