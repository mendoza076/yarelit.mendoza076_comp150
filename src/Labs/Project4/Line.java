package Labs.Project4;

import java.awt.*;

/**
 * Created by yarelit.mendoza076 on 11/22/16.
 */
public class Line extends Shape
{
    //
    // instance variable
    Coordinate end;

    // overloaded constructor
    public Line(Coordinate one,
                Coordinate two)
    {
        super(one);
        end = two;
    }

    // get method
    public Coordinate getEnd()
    {
        Coordinate temp;
        temp = end;
        return temp;
    }

    // set method
    public void setEnd(Coordinate newEnd)
    {
        Coordinate temp = end;
        temp = newEnd;
    }

    // toString method
    public String toString()
    {
        return "The end coordinate is: " + end;
    }

    // equals method
    public boolean equals(Object o)
    {
        boolean result = false;
        if (!(o instanceof Line)) {
            result = false;
        } else {
            Line objLine = (Line) o;
            if (end == objLine.end) {
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }

    // draw method
    public void draw(Graphics g)
    {
        g.fillRect(super.getStart().getX(), super.getStart().getY(), end.getX(), end.getY());
    }
}
