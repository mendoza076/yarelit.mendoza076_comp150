// Lab 5a
// Yarelit Mendoza
// Last modified 9-15-16

import java.util.Scanner;
public class Lab5a
{
  public static void main ( String [] args )
  {
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter a String representing the year (ex: 2016) >");
    
    // Takes a String as input from keyboard, representing a Year.
    String year = scan.next();
    
    
    // A. Check to make sure the input can be converted from a String to an int.
    //    Hint: You can use the isDigit() method from the Character class.
    //    Find a way to check each input        
    // B. If the year entered has two digits, convert the input String to an int using 
    //    methods from the Integer class and add 2000 to the int. The output should result
    //    to the screen     
    // C. If the year entered has four digits, convert the input String to an int using methods
    //    from the Integer class and output the result to the screen   
    // D. If the year entered in contains neither two nor four digits, then output that the year
    //    is not valid.
    
    
    String message = "";
    int num = 0;
    if( year.length() == 2 )
    {
      if (Character.isDigit(year.charAt(0)) && Character.isDigit(year.charAt(1)))
      {
        num = Integer.parseInt(year) + 2000;
        message = "\nYou entered a valid year. \nThe year is: " + num + ".";
      }
      else
      {
        message = "\nYou entered an invalid String.";
      }
    }
    else if ( year.length() == 4 )
    {
      if(Character.isDigit(year.charAt(0)) && Character.isDigit(year.charAt(1)) 
           && Character.isDigit(year.charAt(2)) && Character.isDigit(year.charAt(3)))
      {
        num = Integer.parseInt(year);
        message = "\nYou entered a valid year. \nThe year is: " + num + ".";
      }
      else
      {
        message = "\nYou entered an invalid String.";
      }
    }
    else
    {
      message ="\nYou have entered an invalid String.";
    }
    
    System.out.println("You have entered the String : " + year + message);
    
  }
}