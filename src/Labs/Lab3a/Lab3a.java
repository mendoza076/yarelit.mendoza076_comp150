// Lab 3a
// Yarelit Mendoza
// Last modified 09-06-2016

public class Lab3a
{
  public static void main (String [] args)
  {
    // A. Declare a new String using overloaded constructor. Value = Supercalifragilisticexpialidocious
    String s1 = new String("Supercalifragilisticexpialidocious");
    
    // B. Display String in console
    System.out.println(s1);
    
    // C. Display the length of String to console
    System.out.println("The length is: " + s1.length());
    
    // D. Create new String that is the String from step a will all capital letters
    String s1Upper = s1.toUpperCase();
    
    // E. Display the String to console
    System.out.println(s1Upper);
    
    // F. Create new String that is String from step a with all lower case
    String s1Lower = s1.toLowerCase();
    
    // G. Display String to console
    System.out.println(s1Lower);
    
    // H. Determine the character at location 21.
    char location21 = s1.charAt(21);
    
    // I. Display character in console
    System.out.println(location21);
    
    // J. Determine the index of substring "doc" in original String from Step a
    int index = s1.indexOf("doc");
    
    // K. Display index to console
    System.out.println(index);
    
    // L. Use string concatenation operater and substring method to generate new String from
    //    strings in Steps D and F that has the vallue "superCALI"
    String superWord = s1Lower.substring(0,5);
    String caliWord = s1Upper.substring(5,9);
    
    String newWord = superWord + caliWord;
    
    // M. Display new String to console
    System.out.println(newWord);
  }
}