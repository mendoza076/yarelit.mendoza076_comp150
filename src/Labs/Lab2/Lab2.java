// This is lab 2
// Yarelit Mendoza
// Last Modified 09-01-16

public class Lab2
{
  public static void main (String [] args)
  {
    // A. Define three int variables (5, 7, 19)
    int lengthOne = 5;
    int lengthTwo = 7;
    int lengthThree = 19;
    
    // B. Calculate the average of three lengths
    double average = (double)(lengthOne + lengthTwo + lengthThree)/3;
    // This was for a test. System.out.println(average);
    
    // C. Convert average to millimeters (1 inch = 25.4)
    final double MILLIMETERS = 25.4;
    double avMill = average * MILLIMETERS;
    
    // D. Convert each of the three original lengths from step a to millimeters
    double oneMill = lengthOne * MILLIMETERS;
    double twoMill = lengthTwo * MILLIMETERS;
    double threeMill = lengthThree * MILLIMETERS;
    
    // E. Calculate the average of the three lengths from step d.
    double avTwo = (oneMill + twoMill + threeMill)/3;
    
    // F. Printt the output from step c and step e
    System.out.println( "The average for given lengths is: " + avMill + 
                       " The second method should given the same length: " + avTwo);
    
  }
}