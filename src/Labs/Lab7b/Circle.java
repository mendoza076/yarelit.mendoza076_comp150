package Labs.Lab7b;

/**
 * Created by yarelit.mendoza076 on 10/13/16.
 */
import java.awt.Point;
import java.lang.Math;
public class Circle
{
    // instantiate variables // This is just declaring the variables.  They are instantiated in the constructors.
    private Point center;
    private double radius;

    // CONSTRUCTOR

    public Circle(Point newCenter,
                  double newRadius)
    {
        Point temp = new Point((int)newCenter.getX(), (int)newCenter.getY()); // Good.
        center = temp;
        setRadius(radius);

    }
    
    // Missing default constructor.

    // ACCESSOR METHODS

    public Point getCenter()
    {
        Point temp = new Point((int)center.getX(), (int)center.getY()); // Good
        return center;
    }

    public double getRadus()
    {
        return radius;
    }

    // MUTATOR METHODS

    public void setCenter(Point newCenter)
    {
//        Point temp = new Point((int)newCenter.getX(), (int)newCenter.getY()); // You also need to make a copy here just like in the constructor.
//        center = temp;
        center = newCenter;
    }

    public void setRadius(double newRadius)
    {
        if(newRadius < 0)
        {
            radius = 0;
        }
        else
        {
            radius = newRadius;
        }
    }


    // TOSTRING METHOD
    public String toString()
    {
        return "The area of the circle is: " + area() + "\nThe perimeter of the circle is: " + parameter(); // The toString method should return the a String containing the data from the instance variables.
    }

    // EQUALS METHOD
    public boolean equals(Object o)
    {
        boolean returnBoolean;
        if ( !(o instanceof Circle) )
        {
            returnBoolean = false;
        }
        else
        {
            Circle objCircle = ( Circle ) o;
            if ( center.equals(objCircle.center)
                    && radius == objCircle.radius)
            {
                returnBoolean = true;
            }
            else
            {
                returnBoolean = false;
            }
        }
        return returnBoolean;

    }

    // AREA METHOD
    public double area()
    {
        double area = 2 * Math.PI * radius; // This is the formula for perimeter of a circle.
        return area;
    }

    // PARAMETER METHOD

    public double parameter()
    {
        double parameter = Math.PI * Math.pow(radius, 2);  // This is the formula for area of a circle.
        return parameter;
    }
}
