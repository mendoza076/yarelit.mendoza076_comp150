package Labs.Lab5b;

/**
 * Created by yarelit.mendoza076 on 9/22/16.
 */

import java.util.Scanner;
import java.lang.Math;
public class Lab5b
{
    public static void main ( String [] args)
    {
        //
        // Take three inputs
        Scanner scan = new Scanner (System.in);
        System.out.println("Enter a single digit ( 0 to 9) >");
        int a = scan.nextInt();

        // Switch
        String aString="";
        switch(a)
        {
            case 0:
                aString = "zero";
                break;
            case 1:
                aString = "one";
                break;
            case 2:
                aString = "two";
                break;
            case 3:
                aString = "three";
                break;
            case 4:
                aString = "four";
                break;
            case 5:
                aString = "five";
                break;
            case 6:
                aString = "six";
                break;
            case 7:
                aString = "seven";
                break;
            case 8:
                aString = "eight";
                break;
            case 9:
                aString = "nine";
                break;
            default:
                aString = "Invalid number";
                break;

        }

        System.out.println("Enter a single digit (0 to 9) >");
        int b = scan.nextInt();

        String bString = "";
        switch(b)
        {
            case 0:
                bString = "zero";
                break;
            case 1:
                bString = "one";
                break;
            case 2:
                bString = "two";
                break;
            case 3:
                bString = "three";
                break;
            case 4:
                bString = "four";
                break;
            case 5:
                bString = "five";
                break;
            case 6:
                bString = "six";
                break;
            case 7:
                bString = "seven";
                break;
            case 8:
                bString = "eight";
                break;
            case 9:
                bString = "nine";
                break;
            default:
                bString = "Invalid number";
                break;

        }

        System.out.println("Enter an operation to perform (+, -, *, /, ^)");
        String operation = scan.next();
        char opChar = operation.charAt(0);

        int solution = 0;
        switch(opChar)
        {
            case '+':
                solution = a + b;
                System.out.println(aString + " added to " + bString + " is " + solution);
                break;
            case '-':
                solution = b - a;
                System.out.println(aString + " minus " + bString + " is " + solution);
                break;
            case '*':
                solution = a * b;
                System.out.println(aString + " multiplied by " + bString + " is " + solution);
                break;
            case '/':
                double solution2 = (double)a/b; // You should keep this an int but handle the divide by zero case with a message saying that it was an invalid operation.
                System.out.println(aString + " divided by " + bString + " is " + solution2);
                break;
            case '^':
                solution = (int)Math.pow(a,b);
                System.out.println(aString + " to the power of " + bString + " is " + solution);
                break;
           // Add a default case to handle invalid operations.
        }



    }
}
