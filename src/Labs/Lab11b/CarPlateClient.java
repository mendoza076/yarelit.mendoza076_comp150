package Labs.Lab11b;

/**
 * Created by Yarelit on 12/6/2016.
 */
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CarPlateClient
{
    public static void main (String [] args)
    {
        // instantiate the object
        CarPlate cp1 = new CarPlate("EnterPlateHere", "California", "Pink");
        CarPlate cp2 = new CarPlate("Potato", "Meow", "Boop");
        CarPlate cp3 = new CarPlate("Me", "Ane", "Meh");

        try
        {
            FileOutputStream fos = new FileOutputStream("object", false);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(cp1);
            oos.writeObject(cp2);
            oos.writeObject(cp3);

            oos.close();
            // Good.  Next you need to read this object file.
        }

        catch ( FileNotFoundException fnfe )
        {
            System.out.println("No");
        }
        catch( IOException ioe)
        {
            ioe.printStackTrace();
        }

    }
}
