package Labs.Lab11b;

/**
 * Created by Yarelit on 12/6/2016.
 */
public class CarPlate
{
        // instance variables
        private String plateNumber;
        private String state;
        private String color;

        // overloaded constructor
        public CarPlate(String startPlateNumber, String startState, String startColor)
        {
            setPlateNumber(startPlateNumber);
            setState(startState);
            setColor(startColor);
        }

        // get methods
        public String getPlateNumber()
        {
            String temp = plateNumber;
            return temp;
        }

        public String getState()
        {
            String temp = state;
            return temp;
        }

        public String getColor()
        {
            String temp = color;
            return temp;
        }

        // set methods
        public void setPlateNumber(String newPlateNumber)
        {
            String temp = newPlateNumber;
            plateNumber = temp;
        }

        public void setState(String newState)
        {
            String temp = newState;
            state = temp;
        }

        public void setColor(String newColor)
        {
            String temp = newColor;
            color = temp;
        }

        // toString method
        public String toString()
        {
            return "The license plate number is: " + plateNumber + "\nThe state is: " + state + "\nThe color of the license plate is: " + color;
        }
}
