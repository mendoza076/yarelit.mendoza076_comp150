package Labs.Lab10a;

/**
 * Created by Yarelit on 11/16/2016.
 */
public class Parallelepiped extends Rectangle
{
    private int length;

    public Parallelepiped(int startLength,
                          int startWidth,
                          int startHeight)
    {
        super(startWidth, startHeight);
        length = startLength;
    }

    public int getLength()
    {
        return length;
    }

    public void setLength(int newLength)
    {
        length = newLength;
    }

    public double area()
    {
        return super.area(); //edit // This should be extended from Rectangle area method.
    }

    public double volume()
    {
        double solution = length * super.area(); 
        return solution;
    }
    
    // YOu also need to provide a method that extends the perimeter method of the Rectangle class.
}
