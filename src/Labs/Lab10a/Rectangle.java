package Labs.Lab10a;

/**
 * Created by Yarelit on 11/15/2016.
 */
public class Rectangle
{
    // instance variables
    private int width;
    private int height;

    // overloaded constructor
    public Rectangle(int startWidth,
                     int startHeight)
    {
        width = startWidth;
        height = startHeight;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int newWidth)
    {
        width = newWidth;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int newHeight)
    {
        height = newHeight;
    }

    public double perimeter()
    {
        double solution = 2*(width* height);
        return solution;
    }

    public double area()
    {
        double solution = width*height;
        return solution;
    }
}
