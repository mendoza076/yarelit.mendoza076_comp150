package Labs.Lab10a;

/**
 * Created by Yarelit on 11/16/2016.
 */
public class RectangleClient
{
    public static void main ( String [] args )
    {
        Rectangle r1 = new Rectangle(20, 10);
        System.out.println("Width set to: " + r1.getWidth() + "\nHeight set to: " + r1.getHeight());
        System.out.println("\nArea is: " + r1.area() + ". Parameter is: " + r1.perimeter());

        r1.setWidth(5);
        r1.setHeight(10);

        System.out.println("\nWidth set to: " + r1.getWidth() + "\nHeight set to: " + r1.getHeight());
        System.out.println("\nNew area is: " + r1.area() + "\nNew Parameter is: " + r1.perimeter());

        Parallelepiped p1 = new Parallelepiped(r1.getWidth(), r1.getWidth(), 20);
        System.out.println("\nNew Parallelepiped with: " + " \nWidth: " + p1.getWidth() + "\nHeight: " + p1.getHeight() + "" +
                "\nLength: " + p1.getLength());
        System.out.println("Volume is: " + p1.volume());
    }
}
